import mongoose from 'mongoose';
import { DATABASE_URI } from '../common/config';

const dbConnect = () => {
  mongoose.connect(DATABASE_URI, {
    useNewUrlParser: true,
    useFindAndModify: false,
    useUnifiedTopology: true,
  });
};

export default dbConnect;
