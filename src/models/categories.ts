import mongoose from 'mongoose';
import mongoosePaginate from 'mongoose-paginate-v2';
import mongooseDelete from 'mongoose-delete';

const categorySchema = new mongoose.Schema({
  name: {
    type: String,
    require: true,
  },
  description: {
    type: String,
    default: null,
  },
});

categorySchema.plugin(mongoosePaginate);
categorySchema.plugin(mongooseDelete);

const CategoryModel = mongoose.model('categories', categorySchema);

export default CategoryModel;
