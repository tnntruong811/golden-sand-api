import chalk from 'chalk';
import express from 'express';
import dbConnect from './repositories';
import mongoose from 'mongoose';
import { DATABASE_URI } from './common/config';
import CategoryModel from './models/categories';

const app: any = express();

app.get('/', async (req, res) => {
  res.send('This is the golden sand api!!!!');
});

dbConnect();

const port = process.env.PORT || 3000;

app.listen(port, (error: any) => {
  if (error) {
    return console.error(error);
  }

  mongoose.connection.on('error', (err) => {
    console.error(err);
    console.log('MongoDB connection error. Please make sure MongoDB is running.', chalk.red('✗'));
    process.exit();
  });

  mongoose.connection.once('open', function () {
    console.log('MongoDB connection success. Running at', DATABASE_URI, chalk.green('✓'));
    console.log(`Server is listening on http://0.0.0.0:${port}`, chalk.green('✓'));
    console.log('Press CTRL-C to stop\n');
  });
});
